# <story name>
**To read**: [<link to story opened in Refactories Story UI, just link from browser is good enough>]

**Estimated reading time**: 20 minutes

## Story Outline
Concept of generics

## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #generics
